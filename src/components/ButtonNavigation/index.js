import React from "react";
import arrowLeft from "../../assets/icons/arrow-left-line.png";
import arrowRight from "../../assets/icons/arrow-right-line.png";
import "./style.css";

const ButtonNavigation = (props) => {
    return (
        <button
            className="btn-navigation"
            onClick={props.setArrow === "right" ? props.increaseIndex : props.decreaseIndex}>
            <img src={props.setArrow === "right" ? arrowRight : arrowLeft} />
        </button>
    );
}

export default ButtonNavigation;