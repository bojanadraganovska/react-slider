import React from 'react';
import ReviewCard from './components/ReviewCard';
import "./assets/styles/global.css";
import "./App.css";


function App() {
  let data = [
    {
      img: "https://d1bvpoagx8hqbg.cloudfront.net/originals/how-to-take-a-good-photo-for-your-cv-our-top-tips-9c789ace53283a36f7a3ccf9646be14f.jpg",
      name: "SUSAN ANDERSEN",
      profession: "The Boss",
      description: "Marfa af yr 3 wolf moon kogi, readymade distillery asymmetrical seitan kale chips fingerstache cloud bread mustache twee messenger bag."
    },
    {
      img: "https://images.unsplash.com/flagged/photo-1573603867003-89f5fd7a7576?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cHJvZmVzc2lvbmFsJTIwbWFufGVufDB8fDB8fA%3D%3D&w=1000&q=80",
      name: "PETER SMITH",
      profession: "Product Designer",
      description: "Drinking vinegar polaroid street art echo park, actually semiotics next level butcher master cleanse hammock flexitarian ethical paleo."
    },
    {
      img: "https://media.istockphoto.com/photos/beautiful-smiling-african-ethnicity-businesswoman-picture-id1300304411?b=1&k=20&m=1300304411&s=170667a&w=0&h=7K4_MgZl5jfWL_JD5tF4ZiMVuSqS3MI5nA4TFeTeGa4=",
      name: "MARIA FERGUSON",
      profession: "Office Manager",
      description: "Fingerstache umami squid, kinfolk subway tile selvage tumblr man braid viral kombucha gentrify fanny pack raclette pok pok mustache."
    },
    {
      img: "https://media.istockphoto.com/photos/confident-ginger-businessman-smiling-for-camera-picture-id1153206642?k=20&m=1153206642&s=612x612&w=0&h=bmCpezfV4iYjt5o0qmhwhLWNUe5V7nPnp4AWuBtMQvI=",
      name: "JOHN DOE",
      profession: "Regular Guy",
      description: "Gastropub sustainable tousled prism occupy. Viral XOXO roof party brunch actually, chambray listicle microdosing put a bird on it paleo subway tile squid umami."
    },
  ];

  return (
    <>
      <h1 className='title'><span style={{ color: "#03a9f4" }}>/&nbsp;</span>Reviews</h1>
      <ReviewCard data={data} />
    </>
  );
}

export default App;
