import React, { useEffect, useState } from "react";
import ButtonNavigation from "../ButtonNavigation";
import "./style.css";

const ReviewCard = (props) => {

    const [index, setIndex] = useState(0);
    let setArrow;
    let isMounted = false;

    const increaseIndex = () => {
        setArrow = "right";
        setIndex(index + 1);
        if (index >= 2) {
            setIndex(0);
        }
        isMounted = true;
    };

    const decreaseIndex = () => {
        setIndex(index - 1);
        setArrow = "left";
        if (index <= 0) {
            setIndex(2);
        }
        isMounted = true;
    };

    useEffect(() => {
        const timer = setInterval(() => {
            increaseIndex();
            console.log(index);
        }, 2000)
        if (isMounted) {
            clearInterval(timer);
        }
        return () => clearInterval(timer);
    }, [index]);

    return (
        <>
            <div className="review_content-size">
                <ButtonNavigation increaseIndex={increaseIndex} decreaseIndex={decreaseIndex} setArrow={"left"} />
                <div className="review_content">
                    <img src={props.data[index].img} className="review_person"></img>
                    <span className="review_person-name">{props.data[index].name}</span>
                    <span className="review_person-profession">{props.data[index].profession}</span>
                    <p className="review_person-description">{props.data[index].description}</p>
                </div>
                <ButtonNavigation increaseIndex={increaseIndex} decreaseIndex={decreaseIndex} setArrow={"right"} />
            </div>
        </>
    );
}


export default ReviewCard;  